"use strict";

var _parseCsv = _interopRequireDefault(require("./parseCsv"));

var _olympic = require("./olympic");

var _fileio = require("./fileio");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _parseCsv.default)().then(async data => {
  let athlete_events = data.athlete_events;
  let noc_regions = data.noc_regions;

  try {
    await (0, _fileio.writeFile)("./output/getNoOfOlympicsHosted.json", (0, _olympic.getNoOfOlympicsHosted)(athlete_events));
    await (0, _fileio.writeFile)("./output/getTopCountriesWonMedal.json", (0, _olympic.getTopCountriesWonMedal)(athlete_events, noc_regions, 'NA', 2000, 5));
    await (0, _fileio.writeFile)("./output/getGenderCountPerDecade.json", (0, _olympic.getGenderCountPerDecade)(athlete_events));
    await (0, _fileio.writeFile)("./output/getAverageAgeBoxer.json", (0, _olympic.getAverageAgeBoxer)(athlete_events, "Boxing Men's Heavyweight", 'NA'));
    await (0, _fileio.writeFile)("./output/getMedalistsSeasonwise.json", (0, _olympic.getMedalistsSeasonwise)(athlete_events, 'India', 'NA'));
  } catch (error) {
    console.log(error);
  }
});