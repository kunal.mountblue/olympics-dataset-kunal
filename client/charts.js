let serverUrl = "http://localhost:3000/";

function getData() {
  fetch(serverUrl + "getNoOfOlympicsHosted")
    .then(data => data.json())
    .then((data) => plotNoOfOlympicsHosted(data, "question1"));

  fetch(serverUrl + "getTopCountriesWonMedal")
    .then(data => data.json())
    .then((data) => plotTopCountriesWonMedal(data, "question2"));

  fetch(serverUrl + "getGenderCountPerDecade")
    .then(data => data.json())
    .then(data => plotGenderCountPerDecade(data, "question3"));

  fetch(serverUrl + "getAverageAgeBoxer")
    .then(data => data.json())
    .then(data => plotAverageAgeBoxer(data, "question4"));

  fetch(serverUrl + "getMedalistsSeasonwise")
    .then(data => data.json())
    .then(data => plotMedalistsSeasonwise(data, "question5"));
}
getData();

// Question 1   ****************************************************
function plotNoOfOlympicsHosted(data, elementID) {
  Highcharts.chart(elementID, {
    colors: [
      "#2b908f",
      "#90ee7e",
      "#f45b5b",
      "#7798BF",
      "#aaeeee",
      "#ff0066",
      "#eeaaee",
      "#55BF3B",
      "#DF5353",
      "#7798BF",
      "#aaeeee"
    ],
    chart: {
      type: "column",
      backgroundColor: "#4e4e4e",
      plotBorderColor: "#606063"
    },
    title: {
      text: "Q1 Number of Olympics events hosted in each city",
      style: {
        color: "#FFFFFF",
        fontSize: "18px"
      }
    },
    xAxis: {
      categories: Object.keys(data),
      crosshair: true,
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: "Number of Olympics events",
        style: {
          color: "#FFFFFF",
          fontSize: "18px"
        }
      },
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      showInLegend: false,
      name: "Olympic",
      colorByPoint: "true",
      data: Object.values(data)
    }]
  });
}
/* ----------------X---------------   QUESTION 1 PLOTTED   ----------------X--------------- */

// Question 2   ****************************************************
function plotTopCountriesWonMedal(data, elementID) {
 
  let gold = Object.values(data).map(event => event['Gold']);
  let silver = Object.values(data).map(event => event['Silver']);
  let bronze = Object.values(data).map(event => event['Bronze']);

  Highcharts.chart(elementID, {
    chart: {
      type: 'column',
      backgroundColor: "#4e4e4e",
      plotBorderColor: "#606063"
    },
    title: {
      text: 'Q2 Top 10 countries medalwise after 2000',
      style: {
        color: "#FFFFFF",
        fontSize: "18px"
      }
    },
    xAxis: {
      categories: Object.keys(data),
      crosshair: true,
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    yAxis: {
      min: 0,
      title: {
        text: 'Medals Won '
      },
      stackLabels: {
        enabled: true,
        style: {
          fontWeight: 'bold',
          color: ( // theme
            Highcharts.defaultOptions.title.style &&
            Highcharts.defaultOptions.title.style.color
          ) || 'blue'
        }
      }
    },
    legend: {
      align: 'right',
      x: -30,
      verticalAlign: 'top',
      y: 25,
      floating: true,
      backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || 'white',
      borderColor: '#CCC',
      borderWidth: 1,
      shadow: false
    },

    plotOptions: {
      column: {
        stacking: 'normal',
        dataLabels: {
          enabled: true
        }
      }
    },
    series: [{
      name: "Gold",
      data: gold
    }, {
      name: "Silver",
      data: silver
    }, {
      name: "Bronze",
      data: bronze
    }]
  });
}
/* ----------------X---------------   QUESTION 2 PLOTTED   ----------------X--------------- */

// Question 3   ****************************************************
function plotGenderCountPerDecade(data, elementID) {
  
  let femaleNumbers = Object.values(data).map(ele => ele["F"]);
  let maleNumbers = Object.values(data).map(ele => ele["M"]);
  
  Highcharts.chart(elementID, {
    colors: [
      // "#2b908f",
      // "#90ee7e",
      // "#f45b5b",
      // "#7798BF",
      // "#aaeeee",
      // "#ff0066",
      // "#eeaaee",
      // "#55BF3B",
      // "#DF5353",
      "#7798BF",
      "#aaeeee"
    ],
    chart: {
      type: "column",
      backgroundColor: "#4e4e4e",
      plotBorderColor: "#606063"
    },
    title: {
      text: "Q3 Gender Count Per Decade",
      style: {
        color: "#FFFFFF",
        fontSize: "18px"
      }
    },
    xAxis: {
      categories: Object.keys(data),
      crosshair: true,
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    yAxis: {
      title: {
        text: "Gender Count",
        style: {
          color: "#FFFFFF",
          fontSize: "18px"
        }
      },
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    credits: {
      enabled: false
    },
    series: [{
        //showInLegend: false,
        name: "MALE",
        //colorByPoint: "true",
        data: maleNumbers
      },
      {
        showInLegend: false,
        name: "FEMALE",
        //colorByPoint: "true",
        data: femaleNumbers
      }
    ]
  });
}
/* ----------------X---------------   QUESTION 3 PLOTTED   ----------------X--------------- */

// Question 4   ****************************************************
function plotAverageAgeBoxer(data, elementID) {
  Highcharts.chart(elementID, {
    colors: [
      "#aaeeee"
    ],
    chart: {
      backgroundColor: "#4e4e4e",
      plotBorderColor: "#606063"
    },
    title: {
      text: "Q4 Average Age of Boxing-HeavyWeight Champions",
      style: {
        color: "#FFFFFF",
        fontSize: "18px"
      }
    },
    xAxis: {
      categories: Object.keys(data),
      crosshair: true,
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    yAxis: {
      min: 15,
      title: {
        text: "Average Age of Boxing-HeavyWeight Champions",
        style: {
          color: "#FFFFFF",
          fontSize: "18px"
        }
      },
      labels: {
        style: {
          color: "#E0E0E3"
        }
      }
    },
    plotOptions: {
      column: {
        pointPadding: 0.2,
        borderWidth: 0
      }
    },
    credits: {
      enabled: false
    },
    series: [{
      showInLegend: false,
      name: "Olympic",
      colorByPoint: "true",
      data: Object.values(data)
    }]
  });
}
/* ----------------X---------------   QUESTION 4 PLOTTED   ----------------X--------------- */

// Question 5   ****************************************************
function plotMedalistsSeasonwise(data, elementID) {
    let indianMedalists = '';
    for (let i in data) {
      indianMedalists += `<table>`
      indianMedalists += `<th>${i}</th>`
      indianMedalists += `<tr>`
      for (let j of data[i]) {
        indianMedalists += `<td> ${j}</td>\n`;
      }
      indianMedalists += `</tr>`
      indianMedalists += `</table>`
    }
    document.getElementById(elementID).innerHTML = indianMedalists;
  }
  /* ----------------X---------------   QUESTION 5 PLOTTED   ----------------X--------------- */