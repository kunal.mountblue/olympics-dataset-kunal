import getData from "./parseCsv";

import {
  getNoOfOlympicsHosted,
  getTopCountriesWonMedal,
  getGenderCountPerDecade,
  getAverageAgeBoxer,
  getMedalistsSeasonwise
} from "./olympic";
import {
  writeFile
} from "./fileio";

getData().then(async data => {
  let athlete_events = data.athlete_events;
  let noc_regions = data.noc_regions;
  try {
    await writeFile(
      "./output/getNoOfOlympicsHosted.json",
      getNoOfOlympicsHosted(athlete_events)
    );
    await writeFile(
      "./output/getTopCountriesWonMedal.json",
      getTopCountriesWonMedal(athlete_events, noc_regions, 'NA', 2000, 5)
    );
    await writeFile(
      "./output/getGenderCountPerDecade.json",
      getGenderCountPerDecade(athlete_events)
    );
    await writeFile(
      "./output/getAverageAgeBoxer.json",
      getAverageAgeBoxer(athlete_events, "Boxing Men's Heavyweight", 'NA') 
    );
    await writeFile(
      "./output/getMedalistsSeasonwise.json",
      getMedalistsSeasonwise(athlete_events, 'India', 'NA')
    );

  } catch (error) {
    console.log(error);
  }
});

