// Question 1 : Number of times olympics hosted per City over the years - Bar chart\
function getUniqueGames(events, games) {
  const uniqueGames = events.map(event => event[games]).map((game, index, event_games) => event_games.indexOf(game) === index && index)
    .filter(event => events[event]).map(event => events[event]);
  return uniqueGames;
}

export const getNoOfOlympicsHosted = athlete_events => {
  return getUniqueGames(athlete_events, 'Games').reduce((eventsPerCity, event) => {
    if (eventsPerCity.hasOwnProperty(event.City)) {
      eventsPerCity[event.City]++;
    } else {
      eventsPerCity[event.City] = 1;
    }
    return eventsPerCity;
  }, {});
};

//Question 2 : Top 10 countries which won most medals after 2000 - stacked column - split gold/silver/bronze
function getMedal(events, notMedal, afterYear) {
  return events.filter(event => (event['Medal'] !== notMedal)).filter(event => (event.Year > afterYear));
}

export const getTopCountriesWonMedal = function (athlete_events, noc_regions, medal, afterYear, topRankers) {
  let medalWinnerCountries = getMedal(athlete_events, medal, afterYear);
  let noc = noc_regions.reduce((acc, item) => {
    acc[item.NOC] = item.region;
    return acc;
  }, {});

  let CountriesWithMedals = medalWinnerCountries.reduce((tempCountriesWithMedals, player) => {
    if (tempCountriesWithMedals.hasOwnProperty(noc[player.NOC])) {
      if (!tempCountriesWithMedals[noc[player.NOC]].hasOwnProperty(player.Medal)) {
        tempCountriesWithMedals[noc[player.NOC]][player.Medal] = 1;
      } else {
        tempCountriesWithMedals[noc[player.NOC]][player.Medal] += 1;
      }
      tempCountriesWithMedals[noc[player.NOC]]['Total'] += 1;
    } else {
      tempCountriesWithMedals[noc[player.NOC]] = {};
      if (!tempCountriesWithMedals[noc[player.NOC]].hasOwnProperty(player.Medal)) {
        tempCountriesWithMedals[noc[player.NOC]][player.Medal] = 1;
        tempCountriesWithMedals[noc[player.NOC]]['Total'] = 1;
      }
    }
    return tempCountriesWithMedals;
  }, {});

  var topCountries = Object.entries(CountriesWithMedals).sort((a, b) => {
    return b[1]['Total'] - a[1]['Total'];
  }).slice(0, topRankers).reduce((tempTopCountries,item) => {
    tempTopCountries[item[0]] = item[1];
    return tempTopCountries;
  },{});

  return topCountries;
};

// Question 3 : M/F participation by decade - column chart  
export const getGenderCountPerDecade = athlete_events => {
  let genderParticipationByYear = athlete_events.reduce((gender, item) => {
    if (!gender.hasOwnProperty(item.Games)) {
      gender[item.Games] = {};
      gender[item.Games]['id'] = {};
      gender[item.Games]['id'][item.ID] = 1;
      gender[item.Games]['M'] = 0;
      gender[item.Games]['F'] = 0
      gender[item.Games][item.Sex] += 1;
    } else if (!gender[item.Games]['id'].hasOwnProperty(item.ID)) {
      gender[item.Games]['id'][item.ID] = 1;
      gender[item.Games][item.Sex] += 1;
    }
    return gender;
  }, {});

  let maleFemaleUnique = Object.entries(genderParticipationByYear).reduce((withoutId, item) => {
    withoutId[item[0]] = item[1];
    return withoutId;
  }, {});

  let genderDecadewise = Object.entries(maleFemaleUnique).reduce((genderDecadewiseTEMP, item) => {
    var decStart = parseInt(item[0].substring(0, 3)) * 10;
    var decEnd = parseInt(decStart) + 9;
    var decade = parseInt(decStart) + '-' + decEnd;

    if (!genderDecadewiseTEMP.hasOwnProperty(decade)) {
      genderDecadewiseTEMP[decade] = {};
      genderDecadewiseTEMP[decade]['M'] = item[1]['M'];
      genderDecadewiseTEMP[decade]['F'] = item[1]['F'];
    } else {
      genderDecadewiseTEMP[decade]['M'] += item[1]['M'];
      genderDecadewiseTEMP[decade]['F'] += item[1]['F'];
    }
    return genderDecadewiseTEMP;
  }, {});

  var sortedDecadeGender = Object.entries(genderDecadewise).sort((a, b) => {
    return (a[0].substring(0, 4) - b[0].substring(0, 4));
  }).reduce((tempSortedDecadeGender, item) => {
    tempSortedDecadeGender[item[0]] = item[1];
    return tempSortedDecadeGender;
  }, {});
  return sortedDecadeGender;
}

// Question 4 : Per year average age of athletes who participated in Boxing Men’s Heavyweight - Line
function getBoxingEvents(players, event, notAge) {
  return players.filter(item => (item['Event'] === event) && item['Age'] != notAge);
}

export const getAverageAgeBoxer = function (athlete_events, event, notAge) {
  let averageAge = getBoxingEvents(athlete_events, event, notAge).reduce((result, event) => {
    if (result.hasOwnProperty(event.Year)) {
      result[event.Year]['sumAge'] += parseInt(event.Age);
      result[event.Year]['count'] += 1;
    } else {
      result[event.Year] = {};
      result[event.Year]['averageAge'] = 0;
      result[event.Year]['sumAge'] = parseInt(event.Age);
      result[event.Year]['count'] = 1;
    }
    result[event.Year]['averageAge'] = Math.round(result[event.Year]['sumAge'] / result[event.Year]['count']);
    return result;
  }, {});

  return Object.entries(averageAge).reduce((finalResult, item) => {
    finalResult[item[0]] = Math.round(item[1]['averageAge']);
    return finalResult;
  }, {});
};

// Question 5 : Find out all medal winners from India per season - Table
function returnMedalistsFromCountry(events, team, notMedal) {
  return events.filter(item => (item['Team'] === team) && item['Medal'] !== notMedal);
}

export const getMedalistsSeasonwise = function (athlete_events, team, medal) {
  return returnMedalistsFromCountry(athlete_events, team, medal).reduce((countryMedalistsPerSeason, event) => {
    if (countryMedalistsPerSeason.hasOwnProperty(event.Year)) {
      //if (countryMedalistsPerSeason[event.Year].indexOf(event.Name) === -1) { // don't add people who won in more than one sports in same event
	if (!countryMedalistsPerSeason[event.Year].includes(event.Name)) {      
	  countryMedalistsPerSeason[event.Year].push(event.Name);
      }
    } else {
      countryMedalistsPerSeason[event.Year] = [];
      countryMedalistsPerSeason[event.Year].push(event.Name);
    }
    return countryMedalistsPerSeason;
  }, {});
};
